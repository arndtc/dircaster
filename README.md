# DirCaster
The DirCaster project is maintained by Dr. Bill Baily and can be found at [https://www.dircaster.org/](https://www.dircaster.org/).

## About DirCaster
DirCaster is a PHP script that allows one to very easily start Podcasting MP3 files from their web host. This allows original content creators to easily provide a feed for the Juice Podcast Receiver, jPodder, and other "podcatching" software.  The feed is also fully iTunes compliant!

## DirCaster Documentation
You can find the documentation here: [DirCaster Documentation](doc/README.md)
